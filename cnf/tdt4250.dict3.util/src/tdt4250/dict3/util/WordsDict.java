package tdt4250.dict3.util;

import java.io.IOException;
import java.io.InputStream;

import tdt4250.dict3.api.DictSearchResult;

public abstract class WordsDict {

	private Words words;
	
	protected WordsDict(Words words) {
		this.words = words;
	}

	protected WordsDict(InputStream input) throws IOException {
		this.words = new ResourceWords(input);
	}

	protected String getSuccessMessageStringFormat() {
		return "Yes, %s was found!";
	}

	protected String getFailureMessageStringFormat() {
		return "No, %s was not found!";
	}
	
	public DictSearchResult search(String searchKey) {
		if (words.hasWord(searchKey)) {
			return new DictSearchResult(true, String.format(getSuccessMessageStringFormat(), searchKey), null);
		} else {
			return new DictSearchResult(false, String.format(getFailureMessageStringFormat(), searchKey), null);
		}
	}
}
