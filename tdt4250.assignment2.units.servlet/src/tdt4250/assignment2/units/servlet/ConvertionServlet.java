package tdt4250.assignment2.units.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.http.whiteboard.propertytypes.HttpWhiteboardServletPattern;

import tdt4250.assignment2.units.core.UnitConvertion;
import tdt4250.assignment2.units.core.UnitConvertionResult;

@Component
@HttpWhiteboardServletPattern("/unit/*")
public class ConvertionServlet extends HttpServlet implements Servlet {

	private static final long serialVersionUID = 1L;
	
	private UnitConvertion unitConvertion = new UnitConvertion();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter writer = response.getWriter();
		response.setContentType("text/plain");
		
		String from = request.getParameter("from");
		String val = request.getParameter("value");
		String to = request.getParameter("to");
		if( from == null || to == null || val == null) {
			StringBuffer s = new StringBuffer();
			s.append("Missing resource: \n");
			s.append("URI must contain 3 properties: \n");
			s.append("with keys, 'from', 'value' and 'to'.\n");
			s.append("i.e. /unit?from=C&value=10&to=K");
			response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, s.toString());
			return;
		}else {
			float value;
			try {
				value = Float.parseFloat(val);
			}catch (Exception e) {
				response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, String.format("The value '%s' is not a not a digit.",val));
				return;
			}
			
			UnitConvertionResult result = unitConvertion.search(from, value, to);
			if(!result.isSuccess()) {
				response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, result.getMessage());
				return;
			}else {
				writer.print(result.getMessage());
			}
		}
	}
}