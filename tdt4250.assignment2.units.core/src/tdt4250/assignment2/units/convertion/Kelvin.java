package tdt4250.assignment2.units.convertion;

public class Kelvin extends TempratureType {

	public Kelvin(String fullName, String notation) {
		super(fullName, notation);
	}

	@Override
	public float convert(float f) {
		return f + 273f;
	}
	
	@Override
	public float toCelcius(float f) {
		return f - 273f;
	}

}
