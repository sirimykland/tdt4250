package tdt4250.assignment2.units.convertion;

public class Fahrenheit extends TempratureType {

	public Fahrenheit(String fullName, String notation) {
		super(fullName, notation);
	}

	@Override
	public float convert(float f) {
		return f * 1.8f + 32f;
	}
	
	@Override
	public float toCelcius(float f) {
		return (f-32f)/1.8f;
		
	}
}
