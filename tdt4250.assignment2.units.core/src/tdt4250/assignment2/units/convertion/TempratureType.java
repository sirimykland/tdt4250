package tdt4250.assignment2.units.convertion;

import tdt4250.assignment2.units.core.Unit;

public class TempratureType implements Unit{

	public float convert(float f) {
		return 0.f;
	}
	
	public float toCelcius(float f) {
		return 0.f;
	}

	private String fullName;
	private String notation;
	
	public TempratureType (String fullName, String notation) {
		this.fullName = fullName;
		this.notation = notation;
	}

	@Override
	public String getFullName() {
		return fullName;
	}

	@Override
	public String getNotation() {
		return notation;
	}
	
	
}
