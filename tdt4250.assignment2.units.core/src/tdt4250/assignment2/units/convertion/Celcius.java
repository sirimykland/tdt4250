package tdt4250.assignment2.units.convertion;

public class Celcius extends TempratureType {

	public Celcius(String fullName, String notation) {
		super(fullName, notation);
	}

	@Override
	public float convert(float f) {
		return f;
	}
	
	@Override
	public float toCelcius(float f) {
		return f;	
	}

}
