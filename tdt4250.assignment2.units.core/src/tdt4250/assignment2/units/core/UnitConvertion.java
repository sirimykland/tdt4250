package tdt4250.assignment2.units.core;

import tdt4250.assignment2.units.convertion.Celcius;
import tdt4250.assignment2.units.convertion.Fahrenheit;
import tdt4250.assignment2.units.convertion.Kelvin;
import tdt4250.assignment2.units.convertion.TempratureType;

import tdt4250.assignment2.units.core.UnitConvertionResult;

public class UnitConvertion {


	private static final String DEFAULT_MESSAGE = "Sorry, not able to convert the format";
		
	public UnitConvertionResult search(String from, float value, String to){
		String message = convert(from, value, to);
		boolean success = true;
		if (message == null) {
			message=DEFAULT_MESSAGE;
			success = false;
		}
		return new UnitConvertionResult(success, message);
	}

	public String convert(String from, float value, String to) {
		StringBuffer message = new StringBuffer();
		TempratureType tf = getType(from);
		TempratureType tt = getType(to);
		try {
			float newTemprature = tt.convert(tf.toCelcius(value));
			message.append(String.format("Successfully converted %.2f %s -> %.2f %s", value, from, newTemprature, to));
		}catch(Exception e) {
			message.append(String.format("Could not convert %.2f %s to %s. \n", value, from, tt.getFullName()));
			message.append("Arguments are invalid, try C, F or K\n"+ e.getMessage());
		}
		return message.toString();
	}
	
	public TempratureType getType(String type) {
		switch(type) {
		  case "C":
		    return new Celcius("Celcius","C");
		  case "F":
			return new Fahrenheit("Fahrenheit","F");
		  case "K":
			return new Kelvin("Kelvin","K");
		  default:
			return null;
		}
		
	}
}


