package tdt4250.assignment2.units.core;

import org.osgi.annotation.versioning.ProviderType;

@ProviderType
public interface Unit {
	String getFullName();
	String getNotation();
}