package tdt4250.assignment2.units.core;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public class UnitConvertionResult {

	private final boolean success;
	private final String message;
	
	public UnitConvertionResult(boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
	}
	
	public boolean isSuccess() {
		return success;
	}
	
	public String getMessage() {
		return message;
	}
}
