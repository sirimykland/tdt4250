# Assignment 2 - a simple application using osgi

the folder tdt4250.assignment2.units.servlet contains the servlet.
- contains ConvertionServlet

the folder tdt4250.assignment2.units.core contains two packages: core and convertion.
- core contains
    - the interface Unit
    - the class UnitConvertion
    - the class UnitConvertionResult
- conversion contains
    - the class TempratureType which implements the interface Unit.
    - the classes Celcius, Fahrenheit and Kelvin - all extends TempratureType

An example of the url is: http://localhost:8080/unit?from=F&value=2&to=K